In figura vedete tre cerchi, tangenti a due a due, i cui raggi sono proporzionali a 1,2,3.
Qual è il rapporto tra l’area della zona grigia e quella del cerchio grande?