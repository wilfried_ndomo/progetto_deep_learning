Nel nostro condominio vivono tre individui. Il primo è un tipo
avaro e ogni sera accende la luce della sua camera solo per
un’ora, dalle 20 alle 21. Il secondo soffre invece d’insonnia e
tiene accesa la luce della camera dalle 23 alle 5 del mattino
successivo, per ben sei ore. Il terzo è un tipo curioso: tiene
accesa la luce della sua camera quando è accesa la luce di
almeno uno dei due condomini oppure quando una di queste
luci è stata spenta al massimo da un’ora.
In una giornata (di 24 ore), per quante ore le luci delle tre
camere risultano tutte spente?
Diciottesima
Edizione
Nazionale