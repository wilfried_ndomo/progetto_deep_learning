#!/usr/bin/env python
# coding: utf-8

'''
Author: Wilfried Ndomo
'''

from sklearn.datasets import load_files
import pandas as pd
import numpy as np

def loader(source):
    DATA_DIR = "Esercizi matematici/" + source
    data = load_files(DATA_DIR, encoding="utf-8", decode_error="replace")
    return data

folders = ["train","validation","test"]
data={}

for folder in folders:
    data[folder] = loader(folder)

#creazione dei dizionari per ogni set di dati per poterli salvare su un file csv.
keys = ["exercise","target"]

train = {}
train[keys[0]] = data[folders[0]].data
train[keys[1]] = data[folders[0]].target

validation = {}
validation[keys[0]] = data[folders[1]].data
validation[keys[1]] = data[folders[1]].target

test = {}
test[keys[0]] = data[folders[2]].data
test[keys[1]] = data[folders[2]].target

exercises = []
targets = []

d={}
d[folders[0]] = pd.DataFrame(data=train)
d[folders[1]]  = pd.DataFrame(data=validation)
d[folders[2]]  = pd.DataFrame(data=test)

for i in [0,1,2]:
	exercises = np.concatenate((exercises, d[folders[i]][keys[0]].values), axis=0)
	targets = np.concatenate((targets, d[folders[i]][keys[1]].values), axis=0)
	
dati = {}
dati[keys[0]] = exercises
dati[keys[1]] = targets

(pd.DataFrame.from_dict(data=dati, orient='columns').to_csv('dataset/dataset.csv', header=True))

'''
print("train")
print(len(train[keys[0]]))
print("validation")
print(len(validation[keys[0]]))
print("test")
print(len(test[keys[0]]))
print("dati")
print(len(dati[keys[0]]))
'''



