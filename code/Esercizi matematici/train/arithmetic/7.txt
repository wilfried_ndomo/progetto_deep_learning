Debora e Marco dividono sempre in parti
uguali le loro spese. Ieri Debora è andata
dal macellaio e ha speso 19 euro; Marco, al
supermercato, ha speso oggi 31 euro.
Quanti euro deve dare Debora a Marco
per pareggiare i conti? 