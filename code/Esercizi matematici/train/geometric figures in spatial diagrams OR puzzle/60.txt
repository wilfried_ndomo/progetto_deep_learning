Nel gioco degli scacchi, la regina si
può spostare orizzontalmente o
verticalmente o in diagonale di un
qualunque numero di caselle (a
condizione che non trovi nessun altro
pezzo sul suo percorso). Così, la regina
posta nella mini-scacchiera della figura
di destra può spostarsi in tutte le
caselle grigie.
Annerite quattro caselle della mini-scacchiera della figura di
sotto dove collocate quattro regine in modo che nessuna di
loro con una mossa possa spostarsi nella casella indicata con
X.