
from sklearn.datasets import load_files

container_path = "Esercizi matematici"

data = load_files(container_path, description=None, categories=None, load_content=True, shuffle=True, 
                                    encoding=None, decode_error='strict', random_state=0)

print(data.shape)