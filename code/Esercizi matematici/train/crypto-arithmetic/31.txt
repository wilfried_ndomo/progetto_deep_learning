Riempite le caselle del quadrato con le cifre 1, 2, 3,
4, 5 e in particolare scrivete
sul foglio-risposte le cifre
della prima riga (in alto),
da sinistra verso destra.
Attenzione, però: ognuna di
queste cifre deve figurare una
e una sola volta in ogni riga,
in ogni colonna e in ognuno
dei cinque pezzi in cui il quadrato è stato diviso.