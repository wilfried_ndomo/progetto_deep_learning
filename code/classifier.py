#!/usr/bin/env python
# coding: utf-8
'''
Author: Wilfried Ndomo
'''
import pandas as pd
from deep_learning import *
import numpy as np
import sys

#----------------------------------------------------------------------------------------------------------
keys = ["exercise","target"]
embed_dim = 300
lstm_out = 50
epochs=50
batch_size=128
train_data = pd.read_csv('./dataset/dataset.csv')

data = preprocess_text(train_data[keys[0]])
Y = pd.get_dummies(train_data[keys[1]]).values

vocab_size, X, embedding_matrix = data_analisy(data=data, 
				path_Word2Vec='it/it.bin', 
				embed_dim=embed_dim)

X_train, X_validation, X_test, Y_train, Y_validation, Y_test = train_validation_test_split(X, Y, 
											test_size=177, 
											val_size=70)

earlystopping = EarlyStopping(monitor='val_loss', 
				min_delta=0,
				patience=10, 
				verbose=1, 
				mode='auto')
				
#-------------------MODEL---------------------------------------------------------------------------------------

#***** un solo hiden bidirezionale senza dropout ni regularizzazione
keys2 = ["SensaNormalizer","Dropout","L1", "L2", "DropoutAndL1", "DropoutAndL2"]
mormalisers = {}
mormalisers[keys2[0]] = {'dropout': 0.0, 'L1':False,'L2':False}

mormalisers[keys2[1]] = {'dropout': 0.2, 'L1': False, 'L2': False}

mormalisers[keys2[2]] = {'dropout': 0.0, 'L1': True, 'L2': False}
mormalisers[keys2[3]] = {'dropout': 0.0, 'L1': False,'L2': True}

mormalisers[keys2[4]] = {'dropout': 0.2, 'L1': True,  'L2': False}
mormalisers[keys2[5]] = {'dropout': 0.2, 'L1': False, 'L2':True}

my_key = keys2[5]

vals = mormalisers.get(my_key)
LSTM_history={}
score = {}
categories = ["0 hiden", "1 hiden", "4 hiden","6 hiden"]

LogFile = "./logs/logTrainingCon_" + my_key + ".txt"
ModelsFile = "./models/ModelliCon_" + my_key + ".txt"
f = open(LogFile, "w")

fd = open(ModelsFile, "w")
fd.write("\n" + my_key + " con "+ str(epochs) +"\n")
fd.close()
fd = open(ModelsFile, "a")

for index in [0,1]:
	nhiden = index
	nome_tapa = "Training con " + my_key + " e " + str(nhiden) + " hiden layer LSTM bidirezionali."
			
	LSTM_model = build_models(vocab_size = vocab_size, 
				embed_dim = embed_dim, 
				embedding_matrix = embedding_matrix, 
				dim=X.shape[1], 
				trainable=False, 
				nclasses=7, 
				lstm_out=lstm_out,  
				dropout=vals["dropout"], 
				hidden_layer=nhiden, 
				RL1=vals["L1"], 
				RL2=vals["L2"])
			
	LSTM_model.summary()
	origin_stdout = sys.stdout
	sys.stdout = fd

	print(nome_tapa + "\n\n")
	print(LSTM_model.summary())
	print("\n\n\n")
	sys.stdout = origin_stdout

	LSTM_model.compile(loss='categorical_crossentropy', 
				optimizer='adam', 
				metrics=['accuracy'])

	LSTM_history[categories[index]] = LSTM_model.fit(X_train, Y_train, 
			validation_data=(X_validation, Y_validation), 
			epochs=epochs,
			batch_size=batch_size,
			verbose=2, 
			shuffle=True, 
			callbacks=[earlystopping]
			)
			
	score[categories[index]] =  LSTM_model.evaluate(X_test, Y_test, verbose = 0)
	print('Test loss', score[categories[index]][0])
	print('Test accuracy',score[categories[index]][1])
	
	#******** WRITE ON FILE*****
	
	printOnFile(f=f, history=LSTM_history[categories[index]], score=score[categories[index]], nome_tapa=nome_tapa)
	
	
	
	
nomeFile = "./imgs/acc_loss_" + my_key +".png"

fd.close()
f.close()
plot_history(LSTM_history, score, nomeFile)


