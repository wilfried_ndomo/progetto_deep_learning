Consiste in una striscia illimitata di caselle numerate : 0,1,2,3
…. . C’è anche un numero illimitato di gettoni e, a sua volta,
ogni casella può contenere un numero illimitato di gettoni.
All’inizio del gioco, però, è presente un solo gettone collocato
nella casella con il numero 1. Sono possibili due tipi di mosse :
 si “raddoppia” un gettone (non situato nella casella con
il numero 0) sostituendolo con due gettoni collocati
nelle caselle adiacenti (uno alla sua sinistra, uno alla
sua destra);
 si elimina un gettone, “raggruppandone” due e
sostituendoli con un gettone posto nell’unica casella
compresa tra le due.
Si vince quando si riesce ad avere un solo gettone e questo è
posto nella casella con il numero 2011.
Con quante mosse, al minimo, si può vincere? ( rispondete 0
se pensate che la domanda sia impossibile).