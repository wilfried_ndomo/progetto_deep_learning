Desiderio non nasconde la propria passione per il cioccolato. Ieri ha comprato un certo numero di
cioccolatini, che costavano (ciascuno) 40 centesimi. Oggi ne ha comprato un numero doppio di
un’altra qualità, più buona, al prezzo (ciascuno) di 61 centesimi. Tra ieri e oggi ha speso quasi 10
euro conservando solo pochi spiccioli di resto, meno di 50 centesimi.
Quanti cioccolatini aveva comprato ieri?