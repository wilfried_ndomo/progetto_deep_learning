Sullo schermo del suo computer, Desiderio ha scritto le
cifre 12345. Si diverte poi a inserire tra due cifre
consecutive presenti sul suo schermo ( cioe tra l' 1 e il 2,
tra il 2 e il 3, tra il 3 e il 4, tra il 4 e il 5) un segno "+", un
segno "-" oppure ... niente del tutto. Puo' cosi ottenere,
per esempio, la scrittura 1+2-34+5 oppure 123-45.
Quante sono tutte le "scritture" possibili,?