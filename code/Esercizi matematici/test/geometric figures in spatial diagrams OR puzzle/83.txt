Ogni cerchio rappresenta un' isola delle coste de! Paese della matematica. Ogni isola deve essere collegata a un'altra
da un numero di ponti uguale alla cifra scritta nel cerchio che la rappresenta, in funzione della popolazione che vi
abita. Due isole diverse possono essere collegate solo orizzontalmente o verticalmente tramite un ponte o due ponti. I
ponti sono diritti. Collegare le isole fra loro in modo che si possa andare da ognuna di essa a ogni altra, attraverso i
ponti che le collegano.