La biblioteca di Caldé possiede molti libri. Edoardo ne ha
contati 1988, Dario 2010, Patrizia 2022. “Vi siete sbagliati -
sentenzia Nando – e in particolare quello che ha contato il
numero più vicino a quello esatto si è sbagliato di 3, un altro di
9 e l’altro di 25”.
Quanti libri possiede esattamente la biblioteca di Caldè?