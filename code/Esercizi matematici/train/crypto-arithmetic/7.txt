Nella sottrazione della figura ad ogni simbolo corrisponde sempre una stessa cifra, diversa da quelle corrispondenti 
agli altri simboli.Quale simbolo (B, C o A) nasconde la cifra più grande ?

ABCA -
BBAC =
----
2012