Simone, matto per il bricolage, ha comprato una sega nuova. Quest´attrezzo, ultima novitòà, permette di
realizzare tagli perfettamente piani e possiede una lima tanto fine da rendere trascurabile lo spessore del
taglio. Per provare la sega, Simone prende un cubo di legno ed effettua diversi tagli senza muovere i pezzi.
Ha quindi ottenuto i risultati qui sotto rappresentati (le sei facce del cubo hanno lo stesso aspetto). Quanti
pezzi ha potuto ottenere al massimo?