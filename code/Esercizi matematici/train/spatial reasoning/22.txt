Ecco due immagini di uno stesso cubo; le sue facce sono numerate in modo che le facce opposte abbiano sempre la stessa somma. 
Quale numero è scritto sulla faccia opposta al 13?
Abbiamo disegnato anche lo sviluppo di un cubo, se ti serve lo puoi ritagliare.