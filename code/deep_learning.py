

import re
import keras
import numpy as np
from gensim.models import Word2Vec
from keras.layers import Dropout, Dense, GRU, Embedding, LSTM, Bidirectional, Flatten
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.models import Sequential
from keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

def preprocess_text(X):
	sentence = X.apply(lambda x: re.sub('[^a-zA-Z]', ' ', x)) 		# Remove punctuations and numbers
	sentence = sentence.apply(lambda x: re.sub('[\s+[a-zA-Z]\s+]', ' ', x)) # Single character removal
	sentence = sentence.apply(lambda x: re.sub('\s+', ' ', x)) 		# Removing multiple spaces
	sentence = sentence.apply(lambda x: x.lower())
	
	return sentence
	
def data_analisy(data, path_Word2Vec, embed_dim=5):

	tokenizer = Tokenizer(num_words=5000, split=' ')

	text = data.values
	tokenizer.fit_on_texts(text)
	X = tokenizer.texts_to_sequences(text)

	vocab_size = len(tokenizer.word_index)+1

	X = pad_sequences(X)

	m = Word2Vec.load(path_Word2Vec)
	print('Total %s word.' % vocab_size)
	embedding_matrix = np.zeros((vocab_size, embed_dim))


	for word, i in tokenizer.word_index.items():
		if word in m.wv:
			#print(word)
			embedding_matrix[i] = m.wv[word] # we take here only the embedding of the words we have in our vocabulary
			
	print('Total %s word vectors.' % len(embedding_matrix))
	
	return vocab_size, X, embedding_matrix


def train_validation_test_split(X_dati, Y_dati, test_size=0.2, val_size=0.2):

	X_train_Validation, X_test, Y_train_Validation, Y_test = train_test_split(X_dati, Y_dati, test_size=test_size)
	X_train, X_validation, Y_train, Y_validation = train_test_split(X_train_Validation, Y_train_Validation, test_size=val_size)
	return X_train, X_validation, X_test, Y_train, Y_validation, Y_test
	
def printOnFile(f, history, score, nome_tapa):

	f.write("************ " + nome_tapa + "*******************\n\n\n")
	
	f.write("Model accuracy " + str(history.history['acc'])+ "\n\n")
	f.write("Validation accuracy " + str(history.history['val_acc'])+ "\n\n")
	f.write("Model loss " + str(history.history['loss'])+ "\n\n")
	f.write("Validation loss " + str(history.history['val_loss']) + "\n\n")
	
	f.write("Text accuracy " + str(score[1])+ "\n")
	f.write("Test loss " + str(score[0]) + "\n")
	
	f.write('\n\n')
	
	
def plot_history(histories, score, nomeFile):
    plt.figure(figsize=(16, 10))
   
    for name, history in histories.items():
    	
    	acc = history.history['acc']
    	val_acc = history.history['val_acc']
    	loss = history.history['loss']
    	val_loss = history.history['val_loss']
    	x = range(1, len(acc) + 1)
    	
    	plt.subplot(1, 2, 2)
    	val1 = plt.plot(x, val_loss, "--", label=name.title()+' Validation loss')
    	plt.plot(x, loss, color=val1[0].get_color(), label=name.title()+' Training loss')
    	
    	plt.title('Training and validation loss')
    	plt.xlabel('Epochs')
    	plt.ylabel('Loss')
    	plt.legend()
    	
    	plt.subplot(1, 2, 1)
    	val2 = plt.plot(x, val_acc,"--", label=name.title()+' Validation acc')
    	plt.plot(x, acc, color=val2[0].get_color(), label=name.title()+' Training acc')
    	
    	plt.title('Training and validation accuracy')
    	plt.xlabel('Epochs')
    	plt.ylabel('Accuracy')
    	plt.legend()
    	
    plt.savefig(nomeFile)
    plt.close()

def build_models(vocab_size=5000, embed_dim = 50, embedding_matrix=np.zeros((5000, 200)), dim=200, trainable=False, nclasses=7, lstm_out=5,  dropout=0.5, hidden_layer=1, RL1=False, RL2=False):
    
    if RL1 == True:
        regularizer = keras.regularizers.l1(0.0001)
    elif RL2 == True:
        regularizer = keras.regularizers.l2(0.0001)
    else:
        regularizer = None
    
    #********************LSTM Model************************************************************************
    LSTM_model = Sequential()
    LSTM_model.add(Embedding(vocab_size, embed_dim, weights=[embedding_matrix],input_length=dim, trainable = trainable))
    for i in range(0,hidden_layer):
        LSTM_model.add(Bidirectional(LSTM(lstm_out, return_sequences=True,
                                          activation='tanh',
                                          kernel_regularizer=regularizer,
                                          recurrent_regularizer=regularizer,
                                          dropout=dropout,
                                          recurrent_dropout=dropout)))

    LSTM_model.add(LSTM(lstm_out,
                        kernel_regularizer=regularizer,
                        recurrent_regularizer=regularizer,
                        dropout=dropout,
                        recurrent_dropout=dropout,
                        return_sequences=True)
                   )
    LSTM_model.add(LSTM(lstm_out))
    LSTM_model.add(Dense(nclasses, activation='softmax'))
    
    return LSTM_model
'''
def build_models(vocab_size=5000, embed_dim = 50, embedding_matrix=np.zeros((5000, 200)), dim=200, trainable=False, nclasses=7, lstm_out=5,  dropout=0.5, hidden_layer=1, RL1=False, RL2=False):

	if RL1 == True:
		regularizer = keras.regularizers.l1(0.0001)
	elif RL2 == True:
		regularizer = keras.regularizers.l2(0.0001)
	else:
		regularizer = None
		
#********************LSTM Model************************************************************************
	LSTM_model = Sequential()
	LSTM_model.add(Embedding(vocab_size, embed_dim, weights=[embedding_matrix],input_length=dim, trainable = trainable))
	for i in range(0,hidden_layer):
		LSTM_model.add(Bidirectional(LSTM(lstm_out, return_sequences=True, 
					    activation='tanh', 
					    kernel_regularizer=regularizer,
					    recurrent_regularizer=regularizer,
					    dropout=dropout, 
					    recurrent_dropout=dropout)))
	LSTM_model.add(LSTM(lstm_out, return_sequences=True))
	LSTM_model.add(LSTM(lstm_out))
	LSTM_model.add(Dense(nclasses, activation='softmax'))
	
	return LSTM_model
'''



