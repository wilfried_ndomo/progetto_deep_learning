Tre numeri razionali sono in progressione geometrica ma
la stessa progressione diventa aritmetica quando si
aumenta di 8 il secondo numero. Questa seconda
progressione torna a essere geometrica se si aumenta di 64
l’ultimo suo termine.
Quanto vale il primo numero delle varie progressioni?