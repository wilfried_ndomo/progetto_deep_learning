Augusto ha una vecchia maglietta che ha
disteso sul tavolo. Facendo due tagli rettilinei la
ha tagliata in un certo numero di scampoli di
tessuto. Per esempio, l'avesse tagliata come
indicato nella figura avrebbe ottenuto 4
scampoli: la banda mediana fornisce due
scampoli, in quanto passa per le aperture delle
due maniche e quindi le due facce della
maglietta formano due pezzi separati.
Supponendo che Augusto non sia autorizzato
a spostare gli scampoli tagliati fra un taglio e
un altro né a piegare la maglietta, quanti
scampoli può ottenete al massimo con due
tagli rettilinei? Disegnate questi tagli sulla
figura.