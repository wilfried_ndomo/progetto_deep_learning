Milena si diverte a colorare le caselle della figura a lato,
che assomigliano alle cellette di un alveare.
Le colora stando attenta che ciascuna casella colorata
tocchi esattamente due altre caselle colorate.
Quante caselle può colorare al massimo?