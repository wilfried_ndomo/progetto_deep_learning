La nonna di Federica ha 99 caramelle e
altri 8 nipoti più grandi di Federica.
Distribuisce le caramelle ai nipoti dandone
alcune a Federica, poi una di più al
secondo nipotino, ancora una di più al terzo
e così via sino ad esaurire tutte le
caramelle con il nono nipotino.
Quante caramelle ha avuto Federica? 