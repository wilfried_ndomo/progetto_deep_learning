In   figura   vedete   un   solido   che, quando è poggiato su un tavolo,  ha  tutte  le  sue  facce  (piane) orizzontali
 o verticali.  Quante facce ha, al minimo, questo solido?