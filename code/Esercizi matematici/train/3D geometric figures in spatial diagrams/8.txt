Una scatola di cartone ha la fonna di un tetraedro.
Tagliamo questa scatola secondo tre spigoli che
convergono net medesimo vertice e stirando bene le
facce otteniamo lo sviluppo del tetraedro. Questo sviluppo
e un quadrato ii cui lato misura 30 cm.
Qual era ii volume della scatola di partenza?