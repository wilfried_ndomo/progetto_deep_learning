Con 111 cubetti (uguali tra loro), Desiderio vuole realizzare una
piramide a base
quadrata dove
ogni cubetto, a
partire dal
secondo livello
dal basso,
poggia su
quattro cubetti
del livello
inferiore livello inferiore (come in figura).
L’obiettivo di Desiderio è però di completare, con i 111 cubetti a
sua disposizione, il maggior numero possibile di livelli.
Quanti cubetti rimarranno inutilizzati ?