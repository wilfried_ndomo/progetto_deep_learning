Totò aveva impostato quattro operazioni, ma ha
macchiato con l'inchiostro il risultato
dell'ultima. Le prime tre sono giuste. La quarta
è tale che la divisione dà un risultato intero e il
divisore è diverso da 1. Scrivete una cifra da 1
a 9 in ogni casella, utilizzando tutte le nove
cifre. In ogni linea le cifre scritte in ogni casella
devono essere ordinate da sinistra a destra in
ordine decrescente.