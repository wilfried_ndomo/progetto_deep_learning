Lo scudo del capo della tribù di Mat è piatto e a
forma di decagono regolare. Sulla faccia rivolta
verso il nemico, al fine di gettare su di lui la
cattiva sorte, sono tracciate tre corde che
delimitano un piccolo
triangolo, indicato in
nero nella figura, la cui
superficie è di 21 cm2.
Qual è la superficie in
cm2, arrotondata
all’intero più vicino,
dell’intero scudo?