Considerate la seguente moltiplicazione, sapendo che a lettere uguali corrispondono cifre uguali e a lettere diverse corrispondono cifre diverse:
ILANOM x
4 =
_________
MILANO
Scrivete tutte le possibili soluzioni numeriche per la parola MILANO.