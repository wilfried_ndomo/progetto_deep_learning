Nella lista dei numeri qui sopra riportata eliminate due
numeri la cui somma e 12 e la cui differenza vale 2. Poi
eliminate due numeri la cui somma vale 12 e il cui
prodotto vale 32. 1n seguito eliminate due numeri la cui
differenza vale 7 e il cui prodotto vale 78. Infine
eliminate due numeri tali che quando si divide l'uno per
l'altro il quoziente vale 3 ed il rcsto 2. 
Quale numero rimane?