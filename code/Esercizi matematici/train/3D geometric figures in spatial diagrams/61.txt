Terniate la termite a scavato tre gallerie che attraversano un grande cubo di legno. Esse hanno delle pareti piane e parallele alle facce
del cubo. Ognuna di esse ha una sezione a fonna di 'pentamino' (una fonna ottenuta componendo cinque quadrati uguali adiacenti
l'un l'altro). La loro entrata e indicata in nero nelle figura .

Quanti piccoli cubi (fra quelli che formano ii cubo piu grande, secondo lo schema delle figura) Termate ha cosl tolto dal
grande cubo?