La costruzione che
vedete in figura (in
cui non ci sono
buchi nascosti) è
composta da tanti
cubetti verniciati
all’esterno di rosso,
escluse le 4 facce
che poggiano sul
pavimento.
Quanti cubetti hanno solo 2 facce
rosse?