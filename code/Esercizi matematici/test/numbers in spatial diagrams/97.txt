Renato scrive dei numeri di due cifre ( da 10 a 99, compresi
questi due) tali che : .
- la cifra delle decine e quella delle unita non siano mai
uguali;
la cifra delle decine e quella delle unita non siano mai
consecutive (per esempio, non scrivera i nUDleri 45 o
87, in quanto non soddisfano questa condizione).
Quanti numeri diversi potra al massimo scrivere Renato,
rispettando queste condizioni ?