Trova una serie di quattro numeri interi positivi non nulli tutti diversi , tali che:
- due di loro hanno per somma 45
- due di loro hanno per differenza 45
- due di loro hanno per prodotto 45
- due di loro hanno per 45 quoziente
Se ci sono più risposte, darle tutte. Sul modulo di risposta, scrivere i quattro interi in ordine crescente.