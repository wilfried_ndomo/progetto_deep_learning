Kurt realizza, con dei segmenti rigidi, l’armatura
di un cubo gonfiabile, il cui lato misura 50 cm.
Tale armatura deve essere un pezzo unico e
congiungere tutti i vertici del cubo (i punti indicati
da un circoletto nero nella figura). La figura a
destra mostra un’armatura realizzata con sette
segmenti la cui lunghezza totale è 350 cm. La
figura a destra illustra un’armatura realizzata
con 13 segmenti: al minimo, in cm, arrotondata
all’intero più vicino, qual è la lunghezza totale
di tali segmenti?