Il numero 145541 è un numero palindromo perché lo si può leggere allo stesso modo da sinistra a destra e da destra a sinistra. 
Inoltre, i numeri di due cifre consecutive che si possono leggere in questo numero: 14, 45, 55, 54, 41 sono tutti diversi.
Trovate il numero palindromo più grande che abbia la stessa proprietà e nel quale si possano leggere solo i numeri 1, 2 e 3.