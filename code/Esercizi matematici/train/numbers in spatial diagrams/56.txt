Sergio possiede i quattro gettoni che compaiono in figura, con i
quali ha formato il numero 2010. Disponendoli in altro modo,
può naturalmente formare altri numeri.
Quanti altri numeri di 2, di 3 e di 4 cifre può
complessivamente formare ?
Nota : un numero di 2, 3 o 4 cifre non può cominciare con 0.