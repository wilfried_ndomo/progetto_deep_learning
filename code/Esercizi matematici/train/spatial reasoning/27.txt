Cnossos vuole costruire un labirinto in
questo rettangolo di 60 m per 100 m. Le
barriere che puo montare devono seguire
la quadrettatura ed essere !imitate dagli
incroci della stessa quadrettatura.
Quanti metri di barriere, al massimo,
possono essere sistemate, rispettando le
seguenti regole:
1) da ogni casella si possa raggiungere o
l'entrata o l'uscita;
2) vi sia almeno un percorso. che
dall'entrata raggiunga l'uscita .
