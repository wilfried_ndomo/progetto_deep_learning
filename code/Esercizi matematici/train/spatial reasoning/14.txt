Milena deve colorare le piastrelle che  figurano  nel  disegno.  Vuole  però    fare    in    modo    che    due    esagoni   che 
  hanno   un   lato   in   comune   non   siano   mai   dello   stesso  colore.  Fai  anche  tu  come  Milena,    
  utilizzando    il    minor    numero di colori possibile. Quanti colori ti serviranno?