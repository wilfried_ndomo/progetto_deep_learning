Il tempo concesso per la prova relativa alla categoria C1 è di due ore. Essa è cominciata alle 14h35
e deve finire alle 16h35.
Romano consegna tre quarti d’ora prima della fine. Tommaso consegna un’ora e mezza dopo
l’inizio della prova. Massimo consegna a metà del tempo concesso . Camilla consegna alle 15h45.
Nicola consegna 50 minuti dopo l’inizio della prova.
Classifica i cinque partecipanti secondo l’ordine di consegna (scrivere le iniziali dei nomi).