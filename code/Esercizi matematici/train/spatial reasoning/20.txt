Le pecore di Billy Bill hanno un appetito notevole.
Casi .per economizzare .. J'erba del suo prato, Billy
costruisce per loro un recinto rettangolare. Ha
utilizzato sette segmenti di staccionata lunghi
rispettivamente 11 m., 10 m.; 9 m., 7 m., 4 m., 3 m., 2
m., che ha sistemato uno di seguito all'altro in modo da
formare un rettangolo.
Quale I' area di uno dei possibili rettangoli, espressa
in metri quadrati?