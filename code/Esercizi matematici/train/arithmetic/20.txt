Archie  ha  preso  molto  sul  serio  il  suo  nome  e  ha  deciso  che  da  grande   farà   l’architetto.   Per   il   momento,  
si  limita  a  costruire  delle  torri  con  dei  cubi di  legno. Il primo giorno, ha costruito una torre  di  un  piano  con  un
  solo cubo: la vedete a sinistra, nella fig. 1. Il secondo giorno, ha costruito  una  torre  tre  volte  più  lunga  e  tre  
  volte  più  alta  mettendole   poi   sopra   la   torre   del   primo   giorno   (e   utilizzando quindi in totale 10 cubi) :la
  vedete nella fig. 2. Il  terzo  giorno  ha  costruito  una  torre  lunga  9  cubi  e  alta  9  cubi e, sopra, le ha messo la 
  torre della fig. 2.Quanti cubi in totale utilizza per questa torre del terzo giorno?